var AWS = require("aws-sdk");
AWS.config.update({region: 'ap-southeast-2'});
var Promise = require('es6-promise').Promise;
var ec2 = new AWS.EC2();
var fs = require("fs");
var path = require("path");
var moment = require("moment");
var autoscaling = new AWS.AutoScaling();
var CronJob = require('cron').CronJob;

new CronJob('0 * * * * *', function() {
  /* Searches for all active autoscaling groups and returns a promise,
   * which when resolved will return the list of autoscaling groups in an Array
   */
  function searchAS(){
      return new Promise(function(resolve, reject){
          autoscaling.describeAutoScalingGroups({}, function(err, data) {
              if (err) {
                  reject(err);
              } else {
                  resolve(data.AutoScalingGroups);
              }
          });
      });
  };

  function describeAutoScalingGroups() {
      return new Promise(function(resolve, reject) {
          var check = searchAS().then(function(data){
              // Get the name of the autoscaling that contains the given string
              var ASName = data.map(function(group){
                  if(group.AutoScalingGroupName.match('${AS_NAME}')){
                      return group.AutoScalingGroupName;
                  }
              });
              ASName = ASName.filter(function(n){ return n != undefined });
              return ASName;
          }).then(function(ASName){
              var params = {
                  AutoScalingGroupNames: ASName
              };
              // Get the autoscaling group that matches the name
              autoscaling.describeAutoScalingGroups(params, function(err, data) {
                  if (err) {
                      reject(err);
                  } else {
                      /* resolve with an array of the EC2 instances in the
                       * autoscaling group
                       */
                      resolve(data.AutoScalingGroups[0].Instances);
                  }
              });
          });
      });
  }

  function getInstanceAddress(reservation){
      return reservation.Instances.map(function(instance){
          return instance.PrivateIpAddress;
      });
  }

  function describeInstances(params){
      var IPs = new Array();
      return new Promise(function(resolve, reject){
          ec2.describeInstances(params, function(err, data) {
              if (err) {
                  reject(err);
              } else {
                  data.Reservations.map(function(reservation){
                      IPs = IPs.concat(getInstanceAddress(reservation));
                  });
                  resolve(IPs);
              }
          });
      });
  }

  console.log('starting...');
  describeAutoScalingGroups().then(function(data){
      // get the InstanceId's of all Healthy EC2 instances in the group
      var active_ec2 = data.map(function(status){
          if(status.HealthStatus == 'Healthy' && status.LifecycleState == 'InService') return status.InstanceId
      });

      var params = {
          InstanceIds: active_ec2
      };
      return describeInstances(params);
  })
  .then(function(data){
      // Read HAPROXY configuration file, Update and Reaod service backends
      writeServerFile(data);
  }).catch(function (err) {
      console.error(err);
  });

  function writeServerFile(data) {
      var templateFileName = 'haproxy-template.cfg';
      var outputFileName = '/usr/src/haproxy/list-ips.cfg';
      var port = '3389'

      var templateReadStream = fs.createReadStream(templateFileName);
      var outputFile = fs.createWriteStream(outputFileName);

      templateReadStream.on('end', function () {
          writeLine(0);
      });

      // write the data from the template into the output file
      templateReadStream.pipe(outputFile);

      function writeLine(i) {
          if (i < data.length) {
              var line = 'server ts' + (i + 1) + ' ';
              line += data[i] + ':' + port + '\n';
              fs.appendFile(outputFileName, line, function (err) {
                  if (err) {
                      console.error(err);
                  } else {
                      i++;
                      writeLine(i);
                  }
              });
          }
      }
     console.log('Done...');
  }
}, null, true, 'Australia/Sydney');
