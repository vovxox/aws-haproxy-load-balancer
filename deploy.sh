COMPOSE_FILES="-f /home/ec2-user/docker-compose.yml"

echo "-----------------"
echo "Pulling Images..."

# Pull the image first
docker-compose $COMPOSE_FILES pull

echo "------------------"
echo "Building Images..."

# Build images that need to be built
docker-compose $COMPOSE_FILES build

echo "-----------------------"
echo "Deploying Containers..."

# Then deploy
docker-compose $COMPOSE_FILES up -d as


echo "--------------"
echo "Cleaning up..."

# Finally, clean up!
docker rm -v $(docker ps -a -q -f status=exited)
docker rmi $(docker images -f "dangling=true" -q)

echo "--------------"
echo "Run refresher..."
# Run refrehser
. /home/ec2-user/refresher.sh
